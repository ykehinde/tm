import React, { Component } from 'react';
import Card from './Card.js';
import DatePicker from 'react-date-picker';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      submitted: false,
      title: 'mr',
      name: '',
      status: [],
      income:'',
      dob: new Date(),
      postCode: '',
      doorNumber: '',
      availableCards: [],
      cards: [
        {
          name: 'Anywhere Card',
          apr: '33.9%',
          duration: '0 months',
          pod: '0 months',
          credit: '£300',
        },
        {
          name: 'Student Life',
          apr: '18.9%',
          duration: '0 months',
          pod: '0 months',
          credit: '£1200',
        },
        {
          name: 'Liquid Card',
          apr: '33.9%',
          duration: '12 months',
          pod: '6 months',
          credit: '£3000',
        },
      ]
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleIncome = this.handleIncome.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleChange(event) {
    this.setState({
      name: event.target.value
    });
  }
  handleIncome(event) {
    this.setState({
      income: event.target.value
    });
  }

  handleSelect(event) {
    this.setState({
      title: event.target.value
    });
  }

  handleInputChange(event) {
    const target = event.target;
    const tmpAry = this.state.status;
    let index;

    if (target.checked) {
      tmpAry.push(target.value)
    } else {
      index = tmpAry.indexOf(target.value)
      tmpAry.splice(index, 1)
    }

    this.setState({
      status: tmpAry
    })
  }

  handleSubmit(event) {

    const availableCards = [];

    for (let i = 0; i < this.state.cards.length; i++) {
      const element = this.state.cards[i];
      if (element.name === 'Anywhere Card') {
        availableCards.push(element);
      }
      if (this.state.status.includes('student')) {
        if (element.name === 'Student Life') {
          availableCards.push(element);
        }
      }
      if (this.state.income > 16000) {
        if (element.name === 'Liquid Card') {
          availableCards.push(element);
        }
      }
    }   

    this.setState({
      submitted: true,
      availableCards: availableCards
    })
    event.preventDefault();
  }

  onChange = date => this.setState({
    date
  })

  render() {
    return (
      <div className="App">
      <h1>Crazy Cards Application</h1>
      <form onSubmit={this.handleSubmit}>
        <label>
          <p>Title</p>
          <select value={this.state.value} onChange={this.handleSelect} required>
            <option value="mr">Mr</option>
            <option value="mrs">Mrs</option>
            <option value="miss">Miss</option>
            <option value="ms">Ms</option>
            <option value="dr">Dr</option>
          </select>
        </label>
        <label>
          <p className="label">Full Name:</p>
          <input type="text" value={this.state.name} onChange={this.handleChange} required />
        </label>
        <label>
          <p className="label">Date of Birth</p>
          <DatePicker
          onChange={this.onChange}
          value={this.state.dob}
        />
        </label>
        <label>
          <p className="label">Door Number</p>
          <input type="text" value={this.state.doorNumber} onChange={this.handleChange} required />
        </label>
        <label>
          <p className="label">Post Code</p>
          <input type="text" value={this.state.postCode} onChange={this.handleChange} required />
        </label>
        <p className="label">Employment Status</p>
        <label className="employment-status">
          <p className="label">Full Time</p>
          <input
            name="fullTime"
            type="checkbox"
            value="fullTime"
            checked={this.state.employed}
            onChange={this.handleInputChange} />
        </label>
        <label className="employment-status">
          <p className="label">Part Time</p>
          <input
            name="partTime"
            type="checkbox"
            value="partTime"
            checked={this.state.employed}
            onChange={this.handleInputChange} />
        </label>
        <label className="employment-status">
          <p className="label">Student</p>
          <input
            name="student"
            type="checkbox"
            value="student"
            checked={this.state.employed}
            onChange={this.handleInputChange} />
        </label>
        <label className="income">
          <p className="label">Annual Income</p>
          <span>£</span><input type="number" value={this.state.income} onChange={this.handleIncome} required />
        </label>
        <label>
          <input type="submit" value="Submit" />
        </label>
      </form>

     {this.state.availableCards.length > 0 ?  <h2>Available Cards</h2> : ''}
      {this.state.availableCards.map(function(card, i){
        return (
          <Card key={i} type={card} />
        )
      })}
      </div>
    );
  }
}

export default App;
