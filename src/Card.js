import React, { Component } from 'react';

class Card extends Component {
    render() {
        const card = this.props.type;
        return (
            <div className="card">
                <h2 className="title">{ card.name }</h2>
                <p>Apr: <strong>{card.apr}</strong></p>
                <p>Balance Transfer Offer Duration: <strong>{card.duration}</strong></p>
                <p>Purchase Offer Duration: <strong>{card.pod}</strong></p>
                <p>Credit Available: <strong>{card.credit}</strong></p>
            </div>
        );
    }
}

export default Card;
